DESCRIPTION = "Tools for managing memory technology devices."
SECTION = "base"
DEPENDS = "zlib lzo e2fsprogs util-linux"
HOMEPAGE = "http://www.linux-mtd.infradead.org/"
LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://COPYING;md5=0636e73ff0215e8d672dc4c32c317bb3"

SRC_URI = "file://mtd-utils_1.4.9.tar.gz"

S = "${WORKDIR}/mtd-utils"

#BIN_NAME = flash_erase mtd_debug flash_eraseall nandwrite nanddump
BIN_NAME = bad_block_utils

PR = "r1"

EXTRA_OEMAKE = "'CC=${CC}' 'CFLAGS=${CFLAGS} -I${S}/include -DWITHOUT_XATTR' 'BUILDDIR=${S}'"

do_install () {
	#oe_runmake install DESTDIR=${D} SBINDIR=${sbindir} MANDIR=${mandir} INCLUDEDIR=${includedir}

        install -d ${D}${bindir}/
        install -m 0755 ${S}/${BIN_NAME} ${D}${bindir}/
}

FILES_${PN} += "${bindir}/${BIN_NAME}"

BBCLASSEXTEND = "native"
