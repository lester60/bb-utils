bb-utils is re-write from mtd-utils-1.4.9, it used for mark a good block as a bad block.

## Clean/Compile
```sh
bitbake -c cleanall bb-utils && bitbake -v bb-utils
```

## Command usage
```sh
Usage: nandwrite [OPTION] MTD_DEVICE [INPUTFILE|-]
Mark a block as bad.

  -x, --markbadblock      Mark bad block as bad
  -h, --help              Display this help and exit
      --version           Output version information and exit
```

```sh
./bad_block_utils -x <block_index> <mtd character>
./bad_block_utils -x 2 /dev/mtd4
```
