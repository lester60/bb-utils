#define PROGRAM_NAME "bad_block_utils"

#include <stdio.h>
#include <getopt.h>
#include "common.h"
#include <libmtd.h>

#if 1 // mark block as bad - lester_hu@bandrich.com
#define __SHORT_FILE__ ((strrchr(__FILE__, '/'))?  strrchr(__FILE__, '/') + 1 : __FILE__)
#define DEBUG(message, ...) printf("[LOG][%s:%d] " message, __SHORT_FILE__, __LINE__, ##__VA_ARGS__)
#endif

static const char	*mtd_device;
static int			blockoffset = 0;

static void display_help(int status)
{
	fprintf(status == EXIT_SUCCESS ? stdout : stderr,
"Usage: nandwrite [OPTION] MTD_DEVICE [INPUTFILE|-]\n"
"Mark a block as bad.\n"
"\n"
"  -x, --markbadblock      Mark bad block as bad\n"
"  -h, --help              Display this help and exit\n"
"      --version           Output version information and exit\n"
	);
	exit(status);
}

static void display_version(void)
{
	printf("%1$s " VERSION "\n"
			"\n"
			"Copyright (C) 2003 Thomas Gleixner \n"
			"\n"
			"%1$s comes with NO WARRANTY\n"
			"to the extent permitted by law.\n"
			"\n"
			"You may redistribute copies of %1$s\n"
			"under the terms of the GNU General Public Licence.\n"
			"See the file `COPYING' for more information.\n",
			PROGRAM_NAME);
	exit(EXIT_SUCCESS);
}

static void process_options(int argc, char * const argv[])
{
	int error = 0;

	for (;;) {
		int option_index = 0;
		static const char short_options[] = "hx:";
		static const struct option long_options[] = {
			/* Order of these args with val==0 matters; see option_index. */
			{"version"		, no_argument		, 0, 0	},
			{"help"			, no_argument		, 0, 'h'},
			{"markbadblock"	, required_argument	, 0, 'x'},
			{0, 0, 0, 0},
		};

		int c = getopt_long(argc, argv, short_options,
				long_options, &option_index);
		if (c == EOF)
			break;

		switch (c) {
		case 0:
			switch (option_index) {
			case 0: /* --version */
				display_version();
				break;
			}
			break;
		case 'x':
			blockoffset = atoi(optarg);
			DEBUG("blockoffset : %d\n", blockoffset);
			break;
		case 'h':
			display_help(EXIT_SUCCESS);
			break;
		case '?':
			error++;
			break;
		}
	}

	argc -= optind;
	argv += optind;

	/*
	 * There must be at least the MTD device node positional
	 * argument remaining and, optionally, the input file.
	 */
	
	if (error || argv[0] == NULL)
		display_help(EXIT_FAILURE);

	mtd_device = argv[0];
}

int main(int argc, char * const argv[])
{
	int fd = -1;
	struct mtd_dev_info mtd;
	libmtd_t mtd_desc;
	
	process_options(argc, argv);

	/* Open the device */
	if ((fd = open(mtd_device, O_RDWR)) == -1)
		sys_errmsg_die("%s", mtd_device);

	mtd_desc = libmtd_open();
	if (!mtd_desc)
		errmsg_die("can't initialize libmtd");

	/* Fill in MTD device capability structure */
	if (mtd_get_dev_info(mtd_desc, mtd_device, &mtd) < 0)
		errmsg_die("mtd_get_dev_info failed");
	
	DEBUG("mtd.mtd_num 		: %d\n", mtd.mtd_num);
	DEBUG("mtd.major 		: %d\n", mtd.major);
	DEBUG("mtd.minor 		: %d\n", mtd.minor);
	DEBUG("mtd.type 		: %d\n", mtd.type);
	DEBUG("mtd.eb_size 		: %d\n", mtd.eb_size);
	DEBUG("mtd.min_io_size, : %d\n", mtd.min_io_size);
	DEBUG("mtd.oob_size, 	: %d\n", mtd.oob_size);
	
	DEBUG("Marking block at %d bad\n", blockoffset);
	if (mtd_mark_bad(&mtd, fd, blockoffset)) {
		sys_errmsg("%s: MTD Mark bad block failure", mtd_device);
	} else {
		DEBUG("Operation Complete\n");
	}

	libmtd_close(mtd_desc);
	close(fd);
	exit(EXIT_SUCCESS);
}
